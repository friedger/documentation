# Other Features

Apart from the issue management features, GitMate also provides some merge request management features.

## Code Analysis

GitMate can analyse static code in over 50 languages for errors and bad practices on new or updated
merge requests. Activate GitMate code analysis to review merge requests on your repositories before a
manual review. We provide a detailed analysis report on the merge request which can be used by contributors
to improve their patch.

![image](../images/code_analysis_example.png)

To use this feature activate GitMate's `Code Analysis` plugin on your repository.

!!! note
    GitMate uses [coala](https://coala.io/) for code analysis and requires you to configure a `.coafile` in your repository.
    Refer [coala documentation](https://docs.coala.io/en/latest/Users/Tutorial.html) to learn how to use coala.

## Track manual review of commits

The GitMate Team understands the importance of a manual review.
We always want you to merge good quality code.

GitMate sets a status in the merge request depending on whether or not all commits have been acknowledged
after a manual code review. Maintainers can acknowledge or unacknowledge a commit in merge request by
mentioning a keyword and commit hash in the discussions.

To use this feature activate GitMate's `Track manual review states of commits` plugin.

![image](../images/ack_plugin.png)

## Set Merge Request Labels automatically

GitMate can automatically set labels on merge requests based on review state, merge request size,
mentioned issues and risk.

To use this feature you need to activate

- `Review State` plugin to assign labels based on review state
- `Merge Request Size` plugin to assign labels based on merge request size
- `Sync Issue Labels` plugin to syncronize the merge request labels with the mentioned issues
- `Risk Status` plugin to label merge request with high probability of having bugs.

## Rebase Merge Request

GitMate can rebase the merge request branch, saving the developers valuable time. Just add a
comment with the keyword `rebase` mentioning the authorized username in the discussion.

To use this feature activate the `Rebase Merge Request` plugin on your repository.

## Handle Stale Rerge requests

GitMate helps to prevent accumulation of old merge requests which have not been updated for a long time.

GitMate can identify these merge requests and take actions which include labelling them inactive, unassigning
inactive developer, and finally closing the merge request.

To use this feature activate `Identify Stale MR` plugin on your repository.

## Welcome Contributors

GitMate can welcome contributors by commenting a message you provide on newly opened merge requests.
This feature can be used to give a warm greeting, a thank you, just state the community guidelines or
anything you can think of.

To use this feature activate the `Welcome Contributors` plugin on your repository.
